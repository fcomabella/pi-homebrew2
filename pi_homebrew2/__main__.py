import logging
import logging.config
import os
import sys

import gi

gi.require_version("Gdk", "4.0")
gi.require_version("Gtk", "4.0")
gi.require_version("Rsvg", "2.0")


if __name__ == "__main__":
    from pi_homebrew2.view import PiHomebrew2App

    script_path = os.path.dirname(os.path.realpath(__file__))
    logging.config.fileConfig(f"{script_path}/logging.cfg")
    logging.getLogger("sqlalchemy.engine").setLevel(logging.WARN)

    def start_main_loop():
        app = PiHomebrew2App(application_id="com.pihomebrew.PiHomebrew2")
        app.run(sys.argv)

    start_main_loop()
