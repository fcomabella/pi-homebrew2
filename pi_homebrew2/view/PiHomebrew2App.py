from gi.repository import Gtk  # type: ignore # NOQA
from .windows import MainWindow


class PiHomebrew2App(Gtk.Application):
    def __init__(self, *args, **kwargs):
        Gtk.Application.__init__(self, *args, **kwargs)
        self.connect("activate", self.on_activate)

    def on_activate(self, app):
        self.main_window = MainWindow(application=app)
        self.main_window.present()
