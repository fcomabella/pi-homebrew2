from gi.repository import Gtk  # type: ignore # NOQA
from pi_homebrew2 import __version__


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        Gtk.ApplicationWindow.__init__(self, *args, **kwargs)

        self.set_title(f"Pi Homebrew 2. Version: {__version__}")

        self.set_default_size(600, 400)
        self.maximize()
